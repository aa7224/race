void mx_printchar(char c);

static void print_line(int spaceB, int spaceM, int spaceA, char left_char) {
    for (int i = 0; i < spaceB; i++) {
        mx_printchar(' ');
    }
    mx_printchar('/');
    for (int j = 0; j < spaceM; j++) {
        mx_printchar(' ');
    }
    mx_printchar('\\');
    for (int k = 0; k < spaceA; k++) {
        mx_printchar(' ');
    }
    mx_printchar(left_char);
    mx_printchar('\n');
}

static void mid_piramide(int spaceB, int spaceA, int spaceM, int n) {
    for (int i = 0, len = n / 2 - 1; i < len; i++) {
        print_line(spaceB, spaceM, spaceA, '\\');
        spaceB--;
        spaceM += 2;
        spaceA++;
    }
    spaceA--;
    for (int i = 0, len = n / 2 - 1; i < len; i++) {
        print_line(spaceB, spaceM, spaceA, '|');
        spaceB--;
        spaceM += 2;
        spaceA--;
    }
}

static void top_piramide(int n) {
    for (int i = 0, len = n - 1; i < len; i++) {
        mx_printchar(' ');
    }
    mx_printchar('/');
    mx_printchar('\\');
    mx_printchar('\n');
}

void mx_pyramid(int n) {
    if (n % 2 == 0) {
        int spaceB = n - 2;
        int spaceM = 1;
        int spaceA = 1;

        if (n < 2 || n % 2 != 0) {
           return;
        }
        top_piramide(n);
        mid_piramide(spaceB, spaceA, spaceM, n);
        mx_printchar('/');
        for (int i = 0, len = 2 * n - 3; i < len; i++) {
            mx_printchar('-');
        }
        mx_printchar('\\');
        mx_printchar('|');
        mx_printchar('\n');
    }
}
