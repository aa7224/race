void mx_printchar(char c);


static void print_line(int l, char a) {
    for(int i = 0; i < l; i++)
        mx_printchar(a);
}
static void print_lines_with_symb_beside_and_after(int p, char in, char out) {
    mx_printchar(out);
    print_line(p, in);
    mx_printchar(out);
}

static void addon_for_cubes(int n, int b) {
    print_lines_with_symb_beside_and_after(n*2, ' ', '|');
    print_line(b, ' ');
    mx_printchar('|');
    mx_printchar('\n');
    print_lines_with_symb_beside_and_after(n*2, ' ', '|');
    print_line(b, ' ');
    mx_printchar('+');
    mx_printchar('\n');
    for (int i = n/2  ; i > 0; i--) {
        print_lines_with_symb_beside_and_after(n*2, ' ', '|');
        print_line (--b, ' ');
        mx_printchar('/');
        mx_printchar('\n');
    }
    print_lines_with_symb_beside_and_after(n*2, '-', '+');
    mx_printchar('\n');
}

static void right_sticks(int n, int b) {
    for (int i = 0; i < n/2 - 1; i++){
        print_lines_with_symb_beside_and_after(n*2, ' ', '|');
        print_line(b, ' ');
        mx_printchar('|');
        mx_printchar('\n');
    }
}

void mx_cube(int n) {
    int b = 0;
    if (n > 1) {
        print_line((n/2 + 1), ' ');
        print_lines_with_symb_beside_and_after(n*2, '-', '+');
        mx_printchar('\n');
        for (int i = n/2 ; i > 0; i--) {
           print_line(i, ' ');
           print_lines_with_symb_beside_and_after(n*2, ' ', '/');
            print_line (b++, ' ');
            mx_printchar('|');
            mx_printchar('\n');
        }
        print_lines_with_symb_beside_and_after(n*2, '-', '+');
        print_line(b, ' ');
        mx_printchar('|');
        mx_printchar('\n');
        right_sticks(n, b);
        addon_for_cubes(n, b);
    }
}
